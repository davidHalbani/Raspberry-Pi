#!/bin/bash

##### User settings ######
INSTALL_PATH=/usr/local
###########################

#### Colors #####
Red="\033[0;31m"
Green="\033[0;32m"
Yellow="\033[0;33m"
Cyan="\033[0;36m"
Reset="\033[0m"
#################

echo -e ${Yellow}"Install packages..."${Reset}
sudo apt-get update
sudo apt-get -y build-dep qt5-qmake
sudo apt-get -y build-dep libqt5gui5
sudo apt-get -y build-dep libqt5webengine-data
sudo apt-get -y build-dep libqt5webkit5
sudo apt-get -y install libudev-dev libinput-dev libts-dev libxcb-xinerama0-dev libxcb-xinerama0 gdbserver

sudo mkdir ${INSTALL_PATH}
sudo chown -R pi:pi ${INSTALL_PATH}

sudo mkdir -p /root/.ssh
sudo chmod 700 /root/.ssh
sudo mkdir -p /home/pi/.ssh
sudo chmod 700 /home/pi/.ssh
