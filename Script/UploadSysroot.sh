#!/bin/bash

##### User settings ######
IP=192.168.178.52
SYSROOT_PATH=/home/kampi/Toolchain/RaspberryPi/sysroot
INSTALL_PATH=/usr/local
###########################

#### Colors #####
Red="\033[0;31m"
Green="\033[0;32m"
Reset="\033[0m"
Yellow="\033[0;33m"
Cyan="\033[0;36m"
#################

# Change the symlinks to prevent wrong links in changed files
echo -e ${Yellow}"Change symlinks..."${Reset}
${COMPILER_PATH}/sysroot-relativelinks.py ${COMPILER_PATH}/sysroot

rsync -avz ${SYSROOT_PATH}${INSTALL_PATH}/RaspberryQt root@${IP}:${INSTALL_PATH}

rsync -avz root@${IP}:/lib ${SYSROOT_PATH}
rsync -avz root@${IP}:/usr/include ${SYSROOT_PATH}/usr
rsync -avz root@${IP}:/usr/lib ${SYSROOT_PATH}/usr
rsync -avz root@${IP}:/opt/vc ${SYSROOT_PATH}/opt | tee ${TEMP_PATH}/log/copy_opt_vc.log
